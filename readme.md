Level1: level1

Level2: 12345

Level3: iloveyou

Level4: abdication

level5a: Maybeshewill


# Password Cracking Challenge

### Level 1

This level was extremely simple to crack since the password is the username itself, so simple brute force + a bit of common(or not so much, depending on perspective) sense is all one needs to crack it.

I don't think any sophisticated method would decrease time spent on cracking this one, since it is extremely simple.

### Level 2 & Level 3

These two levels are basically the same challenge spread over two levels. Both require research in most used passwords and both can be found quite easy, with slight difference in difficulty. 

Personally, I had to read the articles and then introduce attempts manually, this is not the best strategy, especially when the pool of passwords is quite big. Hence, a better strategy would be to pull and compile a list of most common passwords and then use a script for automatic checking.

### Level 4

This was a bit(byte) more challenging than the previous one. The pool of possible passwords was about 100k words. I wrote my own script(which did not work super well, but I managed to get the job done) to crack this challeng. First, I had to compile a pool of potential passwords, for this I used both american and english dictionaries provided by OS, then in threads my script would attempt to use those entries to connect to the server.

After consulting with my comrades, I found out that they found a better approach. Instead of using network to connect to the server they have uploaded the script to server and let it work its magic directly on server, thus increasing performance.

### Level 5a

This one was different from the previous levels cracked by me. It required social engineering, namely profile research. I had to access victim's profile and look for his music preferences. I had manually introduced the entries(this of course be done automatically, and save me some time), and the answer turned out to be about the end of the list.

## Conclusions

Everything can be cracked, given the time, resources, and motivation. Therefore, the best practices against h4ck0rz are:

- Never use the same passwords multiple times
- Better use passphrases instead of passwords(length is the key in secure passwords)
- Make sure that your passwords do not contain any information related to you

